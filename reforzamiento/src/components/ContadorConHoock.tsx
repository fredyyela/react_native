import React from 'react'
import { useCounter } from '../hoocks/useCounter';

export const ContadorConHoock = () => {

	const { valor, acumular } = useCounter(100);

	return (
		<>
		<h3>Contador con Hoock: <small>{valor}</small> </h3>

		<button
			className="btn btn-primary"
			onClick={ () => acumular(1) }
		>
			+1
		</button>
		&nbsp;
		<button
			className="btn btn-primary"
			onClick={ () => acumular(-1) }
		>
			-1
		</button>
			
		</>
	)
}
