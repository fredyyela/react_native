//import { TiposBasicos } from './typescript/TiposBasicos';
//import { ObjetosLiterales } from './typescript/ObjetosLiterales';
//import { Funciones } from './typescript/Funciones';
//import { Contador } from './components/Contador';
//import { ContadorConHoock } from './components/ContadorConHoock';
//import { Usuarios } from './components/Usuarios';
//import { Login } from './components/Login';
import { Formularios } from './components/Formularios';


const App = () => {
  return (
    <div className="mt-2">
      <h1>Introdución a TS -React</h1>
      <hr />
      {/*<TiposBasicos /> 
      <ObjetosLiterales /> 
      <Funciones />
      <Contador />
      <ContadorConHoock />
      <Login />
      <Usuarios />*/}
      <Formularios />
    </div>
  )
}

export default App
