
import { useEffect, useState, useRef } from 'react';
import { reqResApi } from '../api/reqRes';
import { ReqResLista, Usuario } from '../interfaces/reqRes';

export const UseUsuarios = () => {


	const [usuarios, setUsuarios] = useState<Usuario[]>([]);

	const paginaRef = useRef(1);

	useEffect (() => {
		//lamado al api
		cargarUsuarios()
		
	}, []);

	const cargarUsuarios = async() => {
		//lamado al api
		/* forma 1
		reqResApi.get<ReqResLista>('/users').then( resp => {
			console.log(resp);
			setUsuarios(resp.data.data);
		}).catch( console.log );
		*/
		const resp = await reqResApi.get<ReqResLista>('/users', {
			params: {
				page: paginaRef.current
			}
		});

		if( resp.data.data.length > 0){
			setUsuarios( resp.data.data);
			
		} else {
			paginaRef.current --;
			alert('No hay mas registros');
		}
	}

	const paginaSiguiente = () => {
		paginaRef.current ++;
		cargarUsuarios();
	}

	const paginaAnterior = () => {
		if (paginaRef.current > 1){
			paginaRef.current --;
			cargarUsuarios();
		}
	}

	

	return {
		usuarios,
		paginaSiguiente,
		paginaAnterior,
	}
	
}
