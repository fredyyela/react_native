
interface Persona {
	nombreCompleto: string;
	edad: number;
	descripcion: {
		pais: string;
		casaNo: number;
	}
}
export const ObjetosLiterales = () => {

	const personas: Persona = {
		nombreCompleto: 'Fredy',
		edad: 35,
		descripcion: {
			pais: 'colombia',
			casaNo: 615
		}
	}
	return (
		<>
			<h3>Objetos Literales</h3>
			<code>
				<pre>
					{ JSON.stringify( personas, null, 2 )}
				</pre>
			</code>
		</>
	)
}
