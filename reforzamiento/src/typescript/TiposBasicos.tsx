
export const TiposBasicos = () => {

	let nombre: string = "Fredy";
	const edad: number = 35;
	const estaActivo:boolean = true;

	const poderes: string[] = ['velocidad', 'Valor', 'Respirar en el agua'];
	return (
		<>
		<h3>Tipos Básicos</h3>
			{ nombre }, { edad }, { estaActivo ? 'activo' : 'no activo'}
			<br />
			{ poderes.join(', ')}
		</>
	)
}
